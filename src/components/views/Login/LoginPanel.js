import React, { Component } from 'react';
import { StyleSheet, Text, View, Animated, Image } from 'react-native';
import BackImage from '../../../Assets/images/loginPanel.jpg';
import LoginForm from './LoginForm';


class LoginPanel extends Component {
    
    state ={
        animfinished: false,
        backeimage: new Animated.Value(0),
        inputForm: new Animated.Value(0)
    }
    
    componentWillReceiveProps(nextProps){
        if(nextProps.show && !this.state.animfinished) {
            Animated.parallel([
                Animated.timing(this.state.backeimage, {
                    toValue: 1,
                    duration: 1000
                }),
                Animated.timing(this.state.inputForm,{
                    toValue:1,
                    duration: 1000
                }).start(
                    this.setState({animfinished: true})
                )
            ])
        }
    }

    render () {
        return (
           <View>
                
                    <Image 
                        source={BackImage} 
                        resizeMode={'contain'}/>
                
                <Animated.View 
                    style={{
                        opacity: this.state.inputForm,
                        top: this.state.inputForm.interpolate({
                            inputRange: [0,1],
                            outputRange: [100, 30]
                        })
                    }}>
                    <LoginForm 
                        platform={this.props.platform}/>
                </Animated.View>
           </View>
        );
    }
}

const styles = StyleSheet.create({
    imageStylePortrait: {
        width: 270,
        height: 150,
    },
    imageStyleLandscape: {
        width: 270,
        height: 0,
    }
})

export default LoginPanel;
import { Navigation } from 'react-native-navigation';
import ConfigureStore from './src/components/store/config';
import {Provider} from 'react-redux';
 
import Login from './src/components/views/Login';
import Home from './src/components/views/Home';
import AddPost from './src/components/views/Admin/AddPost'

const store = ConfigureStore();

//register screen
Navigation.registerComponent(
    "sellitapp.Login", 
    () =>
    Login,
    store,
    Provider,
  );



Navigation.registerComponent(
    "sellitapp.Home", 
    () => 
    Home,
    store,
    Provider,
  );
Navigation.registerComponent(
    "sellitapp.AddPost", 
    () => 
    AddPost,
    store,
    Provider,
  );


export default () => Navigation.startSingleScreenApp({
  screen: {
    screen: 'sellitapp.Login',
    title: 'Login',
    navigatorStyle: {
      navBarHidden: true
    }
  }
})
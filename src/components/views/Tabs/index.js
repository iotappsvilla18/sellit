import { Navigation } from 'react-native-navigation';
import FalseIcon from '../../../Assets/images/circle.png';
import { Dimensions } from 'react-native';

const LoadTabs = () => {
    Navigation.startTabBasedApp({
        tabs: [
            {
                screen: "sellitapp.Home",
                label: "Home",
                title: "Home",
                icon: FalseIcon
            },
            {
                screen: "sellitapp.AddPost",
                label: "Sell It",
                title: "Sell It",
                icon: FalseIcon
            }
        ]
    })
}

export default LoadTabs;
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Input from '../../Utils/Forms/Input';
import ValidationRules from '../../Utils/Forms/ValidationRules';
import LoadTabs from '../Tabs';
import { connect } from 'react-redux';
import {signUp, signIn } from '../../store/actions/user_actions';
import { bindActionCreators } from 'redux';
import { setTokens } from '../../Utils/misc'

class LoginForm extends Component {
    
    state = {
        //form selection 
        type: 'Login',
        action: 'Login',
        actionMode: 'Not a user, Register', 
        //validation
        hasErrors : false,
        //form element
        form: {
            email: {
                value: "",
                valid: false,
                type: "textinput",
                rules: {
                    isRequired: true,
                    isEmail: true
                }
            },
            password: {
                value: "",
                valid: false,
                type: "textinput",
                rules: {
                    isRequired: true,
                    minLength: 6,
                }
            },
            confirmPassword: {
                value: "",
                valid: false,
                type: "textinput",
                rules: {
                    
                    confirmPass: "password",
                }
            }
        }
    }
    //validation
    updateInput =(name, value) => {
        this.setState({hasErrors: false})
        let formCopy = this.state.form;
        formCopy[name].value = value;

        let rules = formCopy[name].rules;
        let valid = ValidationRules(value, rules, formCopy);
        console.log(valid);
        formCopy[name].valid = valid;

        this.setState({form: formCopy})
    }
    //confirm password rule validation
    confirmPassword = () => (
        this.state.type !== 'Login' 
            ? <Input 
                placeholder="Confirm Password"
                type={this.state.form.confirmPassword.type}
                value={this.state.form.confirmPassword.value}
                onChangeText={value => this.updateInput("confirmPassword", value)}
                secureTextEntry/>
            : null
    )
    //change form type login signup
    changeFormType = () => {
        const type = this.state.type;
        this.setState({
            type: type === 'Login' ? 'Register' : 'Login',
            action:  type === 'Login' ? 'Register' : 'Login',
            actionMode: type === 'Login' ? 'Already Registered, Log In' : 'Not a user, Register',
        })
    }
    //submission 
    submitUser = () => {
        let isformValid = true;
        let formToSumit = {};
        const formCopy = this.state.form;
        for (let key in formCopy) {
            if (this.state.type === 'Login'){
                if(key !== 'confirmPassword') {
                    isformValid = isformValid && formCopy[key].valid;
                    formToSumit[key] = formCopy[key].value
                }
            } else {
                isformValid = isformValid && formCopy[key].valid;
                formToSumit[key] = formCopy[key].value
            }
        }
        if (isformValid) {
            if(this.state.type === 'Login'){
                this.props.signIn(formToSumit).then(()=>{
                    // console.log(this.props.User)
                    this.manageAccess()
                })
            } else {
                this.props.signUp(formToSumit).then(()=>{
                    // console.log(this.props.User)
                    this.manageAccess()
                })
            }
        } else {
            this.setState({hasErrors: true});
        }
    }    

    manageAccess = () => {
        if (!this.props.User.userData.uid) {
            this.setState({hasErrors: true})
        } else {
            setTokens(this.props.User.userData, () => {
                this.setState({hasErrors: false});
                LoadTabs();
            })
        }
    }

    formHasErrors = () => (
        this.state.hasErrors 
        ? <View style={styles.error}><Text style={styles.errorText}>Oops! Check Your Info</Text></View>
        : null
    )

    render() {
        return (
            <View style={styles.formInputContainer}>
                <Input  
                    placeholder="Enter your Email"
                    type={this.state.form.email.type}
                    value={this.state.form.email.value}
                    onChangeText={value => this.updateInput("email", value)}
                    autoCapitalize={"none"} 
                    keyboardType={"email-address"}/>
                <Input  
                    placeholder="Enter your Password"
                    type={this.state.form.password.type}
                    value={this.state.form.password.value}
                    onChangeText={value => this.updateInput("password", value)}
                    secureTextEntry/>
                
                {this.confirmPassword()}
                {this.formHasErrors()}

                <View style={this.props.platform === "android" 
                    ? styles.buttonAndroid 
                    : styles.buttonIos}>
                    <Button 
                        title={this.state.action}
                        color="#fd9727"
                        onPress={this.submitUser}/>
                </View>
                <View style={this.props.platform === "android" 
                    ? styles.buttonAndroid 
                    : styles.buttonIos}>
                    <Button 
                        title={this.state.actionMode}
                        color="lightgrey"
                        onPress={this.changeFormType}/>
                </View>
                <View style={this.props.platform === "android" 
                    ? styles.buttonAndroid 
                    : styles.buttonIos}>
                    <Button 
                        title="I will do it later"
                        color="lightgrey"
                        onPress={()=> LoadTabs()}/>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    formInputContainer:{
        minHeight: 400,

    },
    buttonAndroid: {
        marginBottom: 10,
        marginTop: 10,
    },
    buttonIos: {
        marginBottom: 0,
        
    },
    error: {
        marginBottom: 20,
        marginTop: 10,
        alignItems: 'center',
    },
    errorText: {
        color : 'salmon',
        fontFamily: 'Roboto-Black',
    }
})

function mapStateToProps (state) {
    return {
        User: state.User
    }
}

function mapDispatchToProps (dispatch) {
    return bindActionCreators({signUp, signIn}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(LoginForm);
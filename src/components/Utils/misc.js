import {
    Dimensions,
    Platform,
    AsyncStorage
} from 'react-native';

export const SIGNUP = `https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${'AIzaSyCx5sXjvYtVzF8xCK66mYmNANUf01_jfBU'}`

export const APIKEY = 'AIzaSyCx5sXjvYtVzF8xCK66mYmNANUf01_jfBU'

export const SIGNIN= `https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${'AIzaSyCx5sXjvYtVzF8xCK66mYmNANUf01_jfBU'}`

export const REFRESH = `https://securetoken.googleapis.com/v1/token?key=${'AIzaSyCx5sXjvYtVzF8xCK66mYmNANUf01_jfBU'}`

export const getOrientation = (value) => {
    return Dimensions.get("window").height > value ? "Portrait" : "Landscape"
}

export const setOrientationListner = (cb) => {
    return Dimensions.addEventListener("change",cb)
}

export const removeOrientationListner = () => {
    //return Dimentions.removeEventListner("Change")
} 

export const getPlatForm = () => {
    if(Platform.OS === 'ios') {
        return "ios"
    } else {
        return "android"
    }
}

export const getTokens = (cb) => {
    AsyncStorage.multiGet([
        '@sellitApp@token',
        '@sellitApp@refreshToken',
        '@sellitApp@expireToken',
        '@sellitApp@uid',
    ]).then(value => {
        cb(value);
    })
}

export const setTokens = (values, cb) => {
    const dateNow = new Date();
    const expiration = dateNow.getTime() + (3600 * 1000);
    AsyncStorage.multiSet([
        ['@sellitApp@token', values.token],
        ['@sellitApp@refreshToken', values.refToken],
        ['@sellitApp@expireToken', expiration.toString()],
        ['@sellitApp@uid', values.uid],
    ]).then(response => {
        cb();
    })
}